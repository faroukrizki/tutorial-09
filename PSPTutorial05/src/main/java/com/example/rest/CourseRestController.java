package com.example.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.CourseModel;
import com.example.service.CourseService;

@RestController
@RequestMapping("/rest")
public class CourseRestController {

	@Autowired
	CourseService courseService;
	
	@RequestMapping("/course/view/{id_course}")
	public CourseModel viewCourse(@PathVariable(value = "id_course")String id){
		CourseModel c = courseService.selectCourse(id);
		return c;
	}
	
	@RequestMapping("/course/viewall")
	public List<CourseModel> viewAllCourse(){
		List<CourseModel> courses = courseService.selectAllCourses();
		return courses;
	}
}
